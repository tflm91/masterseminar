# Online store as example for a keyboard-accessible web interface

## Introduction

This online store is only intended to illustrate techniques for improving the keyboard accessibility of web interfaces. 
This document describes architecture and implementation and lists keyboard accessibility ratings. 

## Implementation

### Architecture, files and starting

This dummy online store was implemented using HTML, CSS and JavaScript. It is based on the client-server model. 
The server was set up with Node.js and JavaScript; Express was used as the library. The entire server is an HTTP server 
and delivers the required files. It is configured in `app.js`. As the project was implemented with Node.js, the project 
contains a `package.json`. There are three pages on 
the client side. The start page `index.html` shows an accordion containing various product categories. For each product 
category, various products can be displayed with a picture, name and price. The products can be liked and the like can 
be removed again. This changes one button at a time. Another button can be used to add the product to the shopping cart, 
which changes the button and deactivates it. The page `shopping-cart.html` contains a table with different articles which 
can be deleted using the corresponding button. The page `checkout.html` contains a form, where the user can enter 
input and submit it. 

```
|- package.json
|- app.js
|- views
    |- index.html
    |- shopping-cart.html
    |- checkout.html
|- style.css
|- scripts
    |- search.js
    |- index.js
    |- shopping-cart.js
    |- checkout.js
    |- shortcuts.js
```

To use the app, you need Node.js and npm. When you cloned the repo, you first have to install the dependencies: 

```
$ npm install
```

You can run the app with

```
$ node app.js
```

To access the app, open a browser at [http://localhost:8080/](http://localhost:8080/). 

## Keyboard accessibility

This app features different techniques for improving keyboard accessibility. This includes:
* Implementation of a sensible focus management using the HTML attribute `tabindex` and a visible focus indicator. 
* Usage of proper HTML tags especially in the form in the `checkout.html`.
* Usage of sensible keyboard shortcuts with HTML attribute `accesskey `and labeling with `accesskeyLabel`. 
* Usage of a little ARIA for notifications and labeling.
* Usage of a "Skip to content"-Button

The app was tested by the developer. It was found to be very keyboard accessible. The app was also tested with WAVE and 
Lighthouse and scored very well results there. 