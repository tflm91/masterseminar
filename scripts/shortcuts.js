function shortcutButton(button) {
    if (button.accessKeyLabel) {
        button.value += " (" + button.accessKeyLabel + ")"
    }
}

function shortcutLink(link) {
    if (link.accessKeyLabel) {
        link.innerText += " (" + link.accessKeyLabel + ")"
    }
}