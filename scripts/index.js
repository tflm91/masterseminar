document.addEventListener("DOMContentLoaded", () => {
    function toggleLike(likeButton) {
        if (likeButton.querySelector("img").getAttribute("src") === "thumbs-up-regular.jpg") {
            likeButton.querySelector("img").setAttribute("src", "thumbs-up-solid.jpg")
            likeButton.querySelector("img").setAttribute("alt", "Remove like")
        } else {
            likeButton.querySelector("img").setAttribute("src", "thumbs-up-regular.jpg")
            likeButton.querySelector("img").setAttribute("alt", "Like")
        }
    }

    function putIntoCart(cartButton) {
        cartButton.disabled = true;
        cartButton.querySelector("img").setAttribute("src", "cart-shopping-disabled.jpg")
        cartButton.querySelector("img").setAttribute("alt", "Already in shopping cart")
    }

    function initializeCategory(category) {
        const categoryElement = document.getElementById(category)
        const articleList = categoryElement.children[1]
        for (const article of articleList.children) {
            const likeButton = article.children[3]
            const cartButton = article.children[4]
            likeButton.addEventListener("click", () => {toggleLike(likeButton)})
            cartButton.addEventListener("click", () => {putIntoCart(cartButton)})
        }
    }

    initializeCategory("erectable")
    initializeCategory("small")
    initializeCategory("funny")

    const detailsElements = document.querySelectorAll("details > summary")
    detailsElements.forEach((summary, index) => {
        summary.addEventListener("keydown", (evt) => {
            switch (evt.key) {
                case "Home": {
                    detailsElements[0].focus();
                    break;
                }
                case "End": {
                    detailsElements[detailsElements.length - 1].focus();
                    break;
                }
                case "ArrowUp": {
                    if (index > 0) {
                        detailsElements[index - 1].focus()
                    }
                    break;
                }
                case "ArrowDown": {
                    if (index < detailsElements.length - 1) {
                        detailsElements[index + 1].focus()
                    }
                    break;
                }
            }
        })
    })

    function accordionLink(target) {
        const targetId = target.slice(target.indexOf('#') + 1)
        const targetPanel = document.getElementById(targetId)
        if (targetPanel) {
            targetPanel.focus()
            targetPanel.open = true
        }
    }

    document.querySelectorAll("a").forEach((link) => {
        link.addEventListener("click", () => {
            accordionLink(link.getAttribute("href"))
        })
    })

    if (document.location.href.indexOf('#') !== -1) {
        accordionLink(document.location.href)
    }
})