document.addEventListener("DOMContentLoaded", () => {
    const table = document.getElementById("elements")
    const backLink = document.getElementById("back")
    const checkoutLink = document.getElementById("checkout")

    shortcutLink(backLink)
    shortcutLink(checkoutLink)


    for (let i = 1; i < table.rows.length; i++) {
        const row = table.rows[i]
        const deleteCell = row.cells[1];
        const deleteButton = deleteCell.querySelector("button")
        deleteButton.addEventListener("click", () => {
            row.remove()
        })
    }
})