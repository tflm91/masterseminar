document.addEventListener("DOMContentLoaded", () => {
    const checkoutForm = document.getElementById("checkout-form")
    const confirmation = document.getElementById("confirmation")
    const cancelLink = document.getElementById("cancel")
    const submitButton = document.getElementById("submit")

    shortcutLink(cancelLink)
    shortcutButton(submitButton)

    checkoutForm.addEventListener("submit", (evt) => {
        evt.preventDefault()
        evt.stopPropagation()

        checkoutForm.reset()
        confirmation.hidden = false
        confirmation.innerText = "Your order has been successfully submitted"
    })
})