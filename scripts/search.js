document.addEventListener("DOMContentLoaded", () =>  {
    const searchText = document.getElementById("search")
    const searchButton = document.getElementById("search-button")
    const searchResults = document.getElementById("search-results")

    function searchPage(evt) {
        evt.preventDefault()
        evt.stopPropagation()
        const text = searchText.value
        searchResults.hidden = false
        searchResults.innerText = "Results: " + text
    }

    searchButton.addEventListener("click", searchPage)
})