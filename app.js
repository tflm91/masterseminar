const express = require('express')
const path = require('path')
const fs = require('fs')
const app = express()
const port = 8080

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "views", "index.html"))
})

app.get("/style.css", (req, res) => {
    res.sendFile(path.join(__dirname, 'style.css'))
})

function sendSubdirectory(subdirectory) {
    fs.readdir(path.join(__dirname, subdirectory), (err, files) => {
        if (err) {
            console.log(err)
        } else {
            for (let file of files) {
                app.get("/" + file, (req, res) => {
                    res.sendFile(path.join(__dirname, subdirectory, file))
                })
            }
        }
    })
}

sendSubdirectory("views")
sendSubdirectory("scripts")
sendSubdirectory("img")

app.listen(port, () => {
    console.log("App running at port " + port)
})
